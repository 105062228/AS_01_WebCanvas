var draw;
var triangle,rectangle,circle;
var x,y;
var text_input;
var rainbow=0;
var canvas=document.getElementById("cvs");
var context=canvas.getContext("2d");
var imageLoader=document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, true);
var cPushArray = new Array();
var cStep=-1;
var blue=170,red=85,green=255;
var blue_dir=true,red_dir=true,green_dir=true;
var last_x,last_y;
var text_value="Arial";
var hue=0;
cPush();

function mousedown(){
    x=event.offsetX;
    y=event.offsetY;
    //last_x=event.offsetX;
    //last_y=event.offsetY;
    if(text_input){
        var temp="px ";
        temp=temp+/*document.getElementById("text_type").value*/text_value;
        //alert(temp);
        context.font=document.getElementById("text_size").value+temp;
        context.fillStyle=document.getElementById("my_color").value;
        context.fillText(document.getElementById("textin").value,x,y,700-x);
    }
    else{
        context.moveTo(event.offsetX, event.offsetY);
        context.beginPath();
        draw=true;
    }
}
function mousemove(){
    if(triangle&&draw){
        context.lineTo(x,event.offsetY);
        context.lineTo(event.offsetX,event.offsetY);
        context.lineTo(x,y);
        context.fillStyle=context.strokeStyle;
        context.fill();
    }
    else if(rectangle&&draw){
        context.fillRect(x,y,event.offsetX-x, event.offsetY-y);
        context.fillStyle=context.strokeStyle;
        context.stroke();
    }
    else if(circle&&draw){
        context.arc(x,y,distance(event.offsetX,event.offsetY),0,2*Math.PI);
        context.fillStyle=context.strokeStyle;
        context.fill();
    }
    else if(rainbow&&draw){
        //context.moveTo(last_x,last_y);
        //context.lineTo(event.offsetX, event.offsetY);
        //alert("rgb(255,255,"+"255"+")");
        //alert(typeof toString(blue));
        /*context.strokeStyle="rgb("+red.toString()+","+green.toString()+","+blue.toString()+")";
        if(blue_dir){
            blue=blue-1;
            if(blue==0){
                blue_dir=false;
            }
        }
        else{
            blue=blue+1;
            if(blue==255){
                blue_dir=true;
            }
        }
        if(red_dir){
            red=red-1;
            if(red==0){
                red_dir=false;
            }
        }
        else{
            red=red+1;
            if(red==255){
                red_dir=true;
            }
        }
        if(green_dir){
            green=green-1;
            if(green==0){
                green_dir=false;
            }
        }
        else{
            green=green+1;
            if(green==255){
                green_dir=true;
            }
        }
        context.stroke();
        last_x=event.offsetX;
        last_y=event.offsetY;*/
        last_x=event.offsetX;
        last_y=event.offsetY;
        context.strokeStyle=`hsl(${hue}, 100%, 50%)`;
        context.lineWidth=30;
        context.lineCap="round";
        context.beginPath();
        context.moveTo(last_x-3,last_y-3);
        context.lineTo(event.offsetX,event.offsetY);
        context.stroke();
        hue++;
        if(hue>=360){
            hue=0;
        }
    }
    else if(draw){
        context.lineTo(event.offsetX, event.offsetY);
        context.stroke();
    }
}

function distance(a,b){
    var first=Math.pow(a-x,2);
    var second=Math.pow(b-y,2);
    var third=Math.sqrt(first+second);
    return third;
}

function mouseup(){
    draw=false;
    context.closePath();
    cPush();
}

/*function red_onclick(){
    context.strokeStyle="rgb(255,0,0)";
}

function green_onclick(){
    context.strokeStyle="rgb(0,255,0)";
}

function blue_onclick(){
    context.strokeStyle="rgb(0,0,255)";
}

function black_onclick(){
    context.strokeStyle="rgb(0,0,0)";
}*/

function colors_onclick(){
    var first=document.getElementById("my_color").value;
    context.strokeStyle=first;
}

/*function small_onclick(){
    context.lineWidth=1;
}

function mid_onclick(){
    context.lineWidth=3;
}

function big_onclick(){
    context.lineWidth=5;
}*/

function size_onclick(){
    context.lineWidth=document.getElementById("size").value;
}

function eraser_onclick(){
    document.getElementById("cvs").style.cursor="cell";
    context.strokeStyle="#FFFFFF";
    context.lineWidth=10;
    document.getElementById("size").value=10;
    document.getElementById("my_color").value="#FFFFFF";
    triangle=false;
    rectangle=false;
    circle=false;
    text_input=false;
    rainbow=false;
}

function pencil_onclick(){
    document.getElementById("cvs").style.cursor="auto";
    context.strokeStyle="#000000";
    context.lineWidth=1;
    context.lineCap="butt";
    document.getElementById("size").value=1;
    document.getElementById("my_color").value="#000000";
    triangle=false;
    rectangle=false;
    circle=false;
    text_input=false;
    rainbow=false;
}

function refresh_onclick(){
    //location.reload();
    context.clearRect(0,0,700,500);
    cPush();
}

function round_onclick(){
    context.lineCap="round";
}

function square_onclick(){
    context.lineCap="square";
}

function triangle_onclick(){
    document.getElementById("cvs").style.cursor="auto";
    //document.getElementById("my_color").value="#000000";
    //context.strokeStyle="#000000";
    triangle=true;
    rectangle=false;
    circle=false;
    text_input=false;
    rainbow=false;
}

function rectangle_onclick(){
    document.getElementById("cvs").style.cursor="auto";
    //document.getElementById("my_color").value="#000000";
    //context.strokeStyle="#000000";
    rectangle=true;
    triangle=false;
    circle=false;
    text_input=false;
    rainbow=false;
}

function circle_onclick(){
    document.getElementById("cvs").style.cursor="auto";
    //document.getElementById("my_color").value="#000000";
    //context.strokeStyle="#000000";
    circle=true;
    rectangle=false;
    triangle=false;
    text_input=false;
    rainbow=false;
}

function text_onclick(){
    pencil_onclick();
    text_input=true;
    rainbow=false;
}

function handleImage(e){
    var reader=new FileReader();
    reader.onload=function(event){
        var img = new Image();
        img.onload=function(){
            img.width=canvas.width;
            img.height=canvas.height;
            context.drawImage(img,0,0,img.width,img.height);
        }
        img.src=event.target.result;
    }
    cPush();
    reader.readAsDataURL(e.target.files[0]);
}

function cPush() {
    if(cStep<cPushArray.length){ 
        cPushArray.length=cStep+1; 
    }
    cStep++;
    cPushArray.push(context.getImageData(0,0,700,500));
    //document.title = cStep + ":" + cPushArray.length;
}

function cUndo(){
    if(cStep>0){
        cStep--;
        context.putImageData(cPushArray[cStep],0,0);
        //document.title = cStep + ":" + cPushArray.length;
    }
}

function cRedo(){
    if(cStep<cPushArray.length-1){
        cStep++;
        context.putImageData(cPushArray[cStep],0,0);
        //document.title = cStep + ":" + cPushArray.length;
    }
}

function undo_onclick(){
    cUndo();
}

function redo_onclick(){
    cRedo();
}

function rainbow_onclick(){
    rainbow=true;
    document.getElementById("cvs").style.cursor="auto";
}

function text_type_onclick(){
    //alert(1);
    text_value=document.getElementById("op").value;
}

//                                                    __----~~~~~~~~~~~------___
//                                 .  .   ~~//====......          __--~ ~~
//                  -.            \_|//     |||\\  ~~~~~~::::... /~
//                ___-==_       _-~o~  \/    |||  \\            _/~~-
//        __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
//    _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
//  .~       .~       |   \\ -_    /  /-   /   ||      \   /
// /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
// |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
//          '         ~-|      /|    |-~\~~       __--~~
//                      |-~~-_/ |    |   ~\_   _-~            /\
//                           /  \     \__   \/~                \__
//                       _--~ _/ | .-~~____--~-/                  ~~==.
//                      ((->/~   '.|||' -_|    ~~-/ ,              . _||
//                                 -_     ~\      ~~---l__i__i__i--~~_/
//                                 _-~-__   ~)  \--______________--~~
//                               //.-~~~-~_--~- |-------~~~~~~~~
//                                      //.-~~~--\
//                  神獸保佑
//                code無bug!